package com.mastercard.interview;

import java.util.ArrayList;
import java.util.HashSet;

class Permutations {
    /**
     * Building up permutations by inserting letters one by one, into all possible locations.
     * e.g. for ABC, we insert C to index 0, 1, 2 of "AB" and "BA"
     * NOTE implemented using loops instead of recursion to avoid call stack overflow on long strings
     * @param str a string contains all possible letters
     * @return an array of permutations in String format
     */
    public static String[] findAllPermutations(String str) {
        char[] chars = str.toCharArray();

        ArrayList<String> permutations = new ArrayList<>();
        permutations.add(""); // [""] base case

        for (char c : chars) {
            if (c == ' ')
                continue;
            ArrayList<String> old_sub = (ArrayList<String>) permutations.clone();
            permutations.clear();

            for (String base : old_sub) {
                for (int i = 0; i < base.length() + 1; i++) {
                    StringBuilder strBuilder = new StringBuilder(base);
                    strBuilder.insert(i, c);
                    permutations.add(strBuilder.toString());
                }
            }

        }

        // get rid of redundant permutations caused by duplicated letters
        String[] retValue  = (new HashSet<String>(permutations)).toArray(new String[0]);
        return retValue;
    }
}