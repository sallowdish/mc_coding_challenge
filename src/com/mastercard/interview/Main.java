package com.mastercard.interview;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // take input
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a string and press enter to continue...");
        // make sure return value is immutable
        String[] perms = Permutations.findAllPermutations(scanner.nextLine());
        printPermutations(perms);
    }

    private static void printPermutations (String[] perms) {
        for(String p: perms)
            System.out.println(p);
    }
}