package com.mastercard.interview;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class PermutationsTest {
    @Test
    public void testPermutationsOnEmptyString() {
        String[] result = Permutations.findAllPermutations("");
        assertEquals(1, result.length);
        assertEquals("", result[0]);
    }

    @Test
    public void testPermutationsOnSampleString() {
        String[] result = Permutations.findAllPermutations("GOD");
        assertEquals(6, result.length);
        checkPermutationsEquals(new String[] {"DOG", "ODG", "OGD", "DGO", "GDO", "GOD"}, result);
    }

    @Test
    public void testPermutationsOnStringWithDuplicates() {
        String[] result = Permutations.findAllPermutations("GGD");
        assertEquals(3, result.length);
        checkPermutationsEquals(new String[] {"GGD", "GDG", "DGG"}, result);
    }

    @Test
    public void testPermutationsOnStringWithSpace() {
        String[] result = Permutations.findAllPermutations("GO D");
        assertEquals(6, result.length);
        checkPermutationsEquals(new String[] {"DOG", "ODG", "OGD", "DGO", "GDO", "GOD"}, result);
    }

    @Test
    public void testPermutationsOnLongString() {
        String longStr = "0123456789";
        String[] result = Permutations.findAllPermutations(longStr);
        assertEquals(calculateFactorial(longStr.length()), result.length);
    }

    private void checkPermutationsEquals(String[] expected, String[] actual) {
        HashSet<String> hsResult = new HashSet(Arrays.asList(expected));
        for(String p: actual) {
            assertTrue(hsResult.contains(p));
        }
    }

    private static int calculateFactorial(int n) {
        return (n == 1 || n == 0) ? 1 : n * calculateFactorial(n - 1);
    }
}
