PKG_NAME := com.mastercard.interview
PKG_PATH := com/mastercard/interview
SRC_PATH := src/$(PKG_PATH)
TEST_SRC_PATH := test/$(PKG_PATH)
JUNIT_JAR_PATH := test/junit.jar
OUTPUT_DIR := out
OUTPUT_PATH := $(OUTPUT_DIR)/$(PKG_PATH)

all: build_test

build_src:
	@echo "Building source files..."
	-mkdir $(OUTPUT_PATH)
	javac $(SRC_PATH)/Main.java $(SRC_PATH)/Permutations.java -d $(OUTPUT_DIR)

build_test: build_src
	@echo "Building source files..."
	-mkdir $(OUTPUT_PATH)
	javac -cp "$(OUTPUT_DIR):$(JUNIT_JAR_PATH)" $(TEST_SRC_PATH)/PermutationsTest.java -d $(OUTPUT_DIR)

run:
	java --class-path $(OUTPUT_DIR) $(PKG_NAME).Main

run_test:
	java -jar $(JUNIT_JAR_PATH) --class-path $(OUTPUT_DIR) --select-class $(PKG_NAME).PermutationsTest

clean:
	rm -r $(OUTPUT_DIR)